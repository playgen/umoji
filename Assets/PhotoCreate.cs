﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotoCreate : MonoBehaviour
{
    private PhotoGenerator _photoGenerator;

    public IEnumerator Setup()
    {
        _photoGenerator = GameObject.Find("PhotoGenerator").GetComponent<PhotoGenerator>();

        yield return _photoGenerator.GeneratePhotos(SetImage);
    }

    public IEnumerator SetupWithPlayerPhotos(List<TrackedPlayerFaces> playerFaces)
    {
        _photoGenerator = GameObject.Find("PhotoGenerator").GetComponent<PhotoGenerator>();

        yield return _photoGenerator.GeneratePlayerPhotos(playerFaces, SetImage);
    }

    void SetImage(Sprite sprite)
    {
        GetComponent<Image>().sprite = sprite;
        GetComponent<Image>().preserveAspect = true;
    }
}
