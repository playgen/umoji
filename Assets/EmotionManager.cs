﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class EmotionManager : MonoBehaviour
{
    [Serializable]
    public class EmotionImages
    {
        public string Name;
        public Sprite Image;
    }

    public List<EmotionImages> Emotions = new List<EmotionImages>();

    public Sprite GetEmotionSprite(string emotion)
    {
        return Emotions.FirstOrDefault(e => e.Name == emotion).Image;
    }

    public Sprite GetRandomSprite()
    {
        var rand = UnityEngine.Random.Range(0, Emotions.Count);

        return Emotions[rand].Image;
    }

    public EmotionImages GetRandomEmotionImage()
    {
        var rand = UnityEngine.Random.Range(0, Emotions.Count);

        return Emotions[rand];
    }
}
