﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TEST_SceneTransition : MonoBehaviour
{
    public static Text FaceText;
    public static Text FaceText2;
    public static Text PointsText;

    public string Scene1;
    public string Scene2;

    public bool s1, s2;

    private string currentScene = "";

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            // reset
            UnloadScene(currentScene);
            currentScene = "";
        }
        if (Input.GetKeyDown(KeyCode.Alpha1) || s1)
        {
            s1 = false;
            // load scene 1
            if (currentScene != "")
            {
                UnloadScene(currentScene);
                currentScene = "";
            }
            LoadScene(Scene1);
            currentScene = Scene1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3) || s2)
        {
            s2 = false;
            // load scene 2
            if (currentScene != "")
            {
                UnloadScene(currentScene);
                currentScene = "";
            }
            LoadScene(Scene2);
            currentScene = Scene2;
        }
    }

    public void Load0()
    {
        // reset
        UnloadScene(currentScene);
        currentScene = "";
    }
    public void Load1()
    {
        // load scene 1
        if (currentScene != "")
        {
            UnloadScene(currentScene);
            currentScene = "";
        }
        LoadScene(Scene1);
        currentScene = Scene1;
    }
    public void Load2()
    {
        s2 = false;
        // load scene 2
        if (currentScene != "")
        {
            UnloadScene(currentScene);
            currentScene = "";
        }
        LoadScene(Scene2);
        currentScene = Scene2;
    }


    public static void FaceFound()
    {
        if (FaceText == null)
        {
            FaceText = GameObject.Find("FaceText").GetComponent<Text>();
            FaceText2 = GameObject.Find("FaceText2").GetComponent<Text>();
        }
        FaceText.text = "Face Found";
        FaceText2.text = "Face Found";
    }

    public static void FaceLost()
    {
        if (FaceText == null)
        {
            FaceText = GameObject.Find("FaceText").GetComponent<Text>();
            FaceText2 = GameObject.Find("FaceText2").GetComponent<Text>();
        }
        FaceText.text = "Face Lost";
        FaceText2.text = "Face Lost";
    }

    public static void FacePointData(List<Vector2> points)
    {
        var str = "";
        for (var i = 0; i < points.Count; i++)
        {
            str += "Point " + i + " [" + points[i].x.ToString("0.00") + "," + points[i].y.ToString("0.00") +"],\n";
        }
        if (PointsText == null)
        {
            PointsText = GameObject.Find("PointsText").GetComponent<Text>();
        }

        PointsText.text = str;
    }

    public static void ShowFacePoints(List<Vector2> points)
    {
        Debug.DrawLine(points[36], points[45], Color.magenta, .1f, false);
        var z = -2;
        for (var i = 1; i < points.Count; i++)
        {
            Debug.DrawLine(points[i-1], points[i], Color.yellow, .1f, false);
        }
    }
    public static void FacePointData(string points)
    {
        if (PointsText == null)
        {
            PointsText = GameObject.Find("PointsText").GetComponent<Text>();
        }

        PointsText.text = points;
    }

    private void LoadScene(string scene)
    {
        SceneManager.LoadScene(scene, LoadSceneMode.Additive);
    }

    private void UnloadScene(string scene)
    {
        SceneManager.UnloadSceneAsync(scene);
    }
}
