﻿using UnityEngine;
using UnityEngine.UI;
   
/// <summary>
/// Force unity to display text on one line rather than multiple, simply replace spaces with non-breaking space
/// </summary>
public class SingleLineTextHack : MonoBehaviour
{
	void Start ()
	{
	    var text = GetComponent<Text>();
	    text.text = text.text.Replace(" ", "\u00a0");
	}
	
}
