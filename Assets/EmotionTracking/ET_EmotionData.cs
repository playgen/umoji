﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EmotionTracking
{
    public class TrackedEmotionData
    {
        public string Emotion;
        public string Data;
        public string Image;
    }

    public class ET_EmotionData : MonoBehaviour
    {
        public readonly List<TrackedEmotionData> TrackedEmotion = new List<TrackedEmotionData>();

        public TrackedEmotionData AddData(string emotion, string data, Texture2D image)
        {
            var tracked = new TrackedEmotionData
            {
                Emotion = emotion,
                Data = data,
                Image = ImageToString(image)
            };

            //OutputObject(tracked);

            TrackedEmotion.Add(tracked);
            return tracked;
        }

        private string ImageToString(Texture2D tex)
        {
            //TextureScale.Bilinear(tex, 256, 256);
            var bytes = tex.EncodeToJPG();
            return Convert.ToBase64String(bytes);
        }

        private void OutputObject(TrackedEmotionData data)
        {
            Debug.Log("Emotion: " + data.Emotion 
                        + "\nData" + data.Data
                        + "\nImage" + data.Image);
        }
    }

}
