﻿using System.Collections;
using System.Collections.Generic;
using DlibFaceLandmarkDetectorExample;
using UnityEngine;
using UnityEngine.UI;

namespace EmotionTracking
{
    public class ET_Manager : MonoBehaviour
    {
        public Text TestEmotion;
        public Text TestText;
        public Image TestImage;

        private Text _emotionText;
        private Text _emotionDescriptionText;
        private Text _emotionProgressText;
        private Image _emojiImage;

        public RawImage PlayerImage;
        public WebCamFeed WebCamFeed;

        public GameObject StartScreen;
        public GameObject EndScreen;

        private ET_Config _etConfig;
        private ET_Feedback _etFeedback;
        private ET_EmotionData _etEmotionData;

        private int index = 0;

        // Our snapshot variables
        private CameraScreenShot _cameraScreenShot;

        void Awake()
        {
            _etConfig = new ET_Config();
            _etFeedback = GetComponent<ET_Feedback>();
            _etEmotionData = new ET_EmotionData();

            _etConfig.Set();

            _cameraScreenShot = GameObject.Find("ScreenShotCamera").GetComponent<CameraScreenShot>();

            StartScreen.SetActive(true);
            EndScreen.SetActive(false);
        }

        public void StartGame()
        {
            StartScreen.SetActive(false);
            ShowNextEmotion();
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        public void Done()
        {
            // Get Snapshot of data and photo
            var title = _etConfig.EmotionsToTrack[index - 1].Name;
            var data = WebCamFeed.GetFacePointData();
            var image = WebCamFeed.GetCurrentTexture();

            // Test display
            if (TestText != null && TestImage != null && TestEmotion != null)
            {
                var sprite = Sprite.Create(image, new Rect(0, 0, image.width, image.height), Vector2.zero);

                TestText.text = data;
                TestImage.sprite = sprite;
                TestEmotion.text = title;
            }

            var ed = _etEmotionData.AddData(title,data,image);

            _etFeedback.SendFeedback(new ET_UploadData(ed));

            // Go To next emotion in list
            ShowNextEmotion();
        }

        private void ShowNextEmotion()
        {
            if (index >= _etConfig.EmotionsToTrack.Count)
            {
                // End of emotion list
                EndScreen.SetActive(true);
            }
            else
            {
                // Show next emotion
                var emotion = _etConfig.EmotionsToTrack[index];
                ShowEmotion(emotion, (index+1) + "/" + _etConfig.EmotionsToTrack.Count);
                index += 1;
            }
        }

        private void ShowEmotion(Emotions emotion, string progress)
        {
            if (_emojiImage == null)
            {
                _emojiImage = GameObject.Find("EmotionEmoji").GetComponent<Image>();
            }
            if (_emotionText == null)
            {
                _emotionText = GameObject.Find("EmotionText").GetComponent<Text>();
            }
            if (_emotionDescriptionText == null)
            {
                _emotionDescriptionText = GameObject.Find("EmotionDescription").GetComponent<Text>();
            }
            if (_emotionProgressText == null)
            {
                _emotionProgressText = GameObject.Find("EmotionProgress").GetComponent<Text>();
            }

            _emojiImage.sprite = emotion.Sprite;
            _emotionText.text = emotion.Name.Replace("_", " ");
            _emotionDescriptionText.text = emotion.Description;
            _emotionProgressText.text = progress;
        }
    }
}
