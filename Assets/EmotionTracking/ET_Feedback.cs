﻿using System.Collections;
using System.Security.Policy;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Events;

namespace EmotionTracking
{
    public class ET_Feedback : MonoBehaviour
    {
        [SerializeField] private string _url = "http://localhost:51466/api";

        [SerializeField] private string _getAllExtension = "/";
        [SerializeField] private string _getByIdExtension = "/";
        [SerializeField] private string _getByEmotionExtension = "/for/";

        [SerializeField] private string _postExtension = "/";

        public ET_Feedback()
        {

        }

        public void SendFeedback(ET_UploadData data)
        {
            var dataString = JsonConvert.SerializeObject(data);

            StartCoroutine(Post(_postExtension, dataString));
        }
        
        public IEnumerator PostResult(string extension, string body, UnityAction<string> successCallback)
        {
            var cd = new CoroutineWithData(this, Post(extension, body));

            yield return cd.coroutine;

            if (successCallback != null)
            {
                successCallback("" + cd.result);
            }
        }
        private IEnumerator Post(string urlExtension, string body)
        {
            var form = new WWWForm();

            var headers = form.headers;
            var rawData = form.data;

            headers["Content-Type"] = "application/json";

            rawData = System.Text.Encoding.UTF8.GetBytes(body);

            var www = new WWW(_url + urlExtension, rawData, headers);

            yield return www;

            yield return www.text;
        }

        public IEnumerator GetResult(string url, UnityAction<string> successCallback)
        {
           var cd = new CoroutineWithData(this, Get(url));

            yield return cd.coroutine;

            if (successCallback != null)
            {
                successCallback("" + cd.result);
            }
        }
        private IEnumerator Get(string urlExtension)
        {
            var form = new WWWForm();

            var headers = form.headers;

            var www = new WWW(_url + urlExtension, null, headers);

            yield return www;

            yield return www.text;
        }
    }
}
