﻿using UnityEngine;

namespace EmotionTracking
{
    public class ET_UploadData
    {

        public string Name;
        public string Image;
        public string Data;

        public string DeviceId;
        public string Version;

        public ET_UploadData()
        {
            DeviceId = SystemInfo.deviceUniqueIdentifier;
            Version = Application.version;
        }

        public ET_UploadData(TrackedEmotionData data)
        {
            Name = data.Emotion;
            Image = data.Image;
            Data = data.Data;

            DeviceId = SystemInfo.deviceUniqueIdentifier;
            Version = Application.version;
        }

    }
}
