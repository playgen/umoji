﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace EmotionTracking
{
    [Serializable]
    public class Emotions
    {
        public string Name;
        public string Description;
        public Sprite Sprite;
    }

    public class Data
    {
        public RawEmotioData[] Files;
    }

    [Serializable]
    public class RawEmotioData
    {
        public string Name;
        public string Description;
        public string FileName;
    }

    public class ET_Config : MonoBehaviour
    {
        public readonly List<Emotions> EmotionsToTrack = new List<Emotions>();

        public void Set()
        {
            //var files = new Data() [ new RawEmotioData { "", "", ""}, new RawEmotioData { "", "", ""} ]

            var names = Resources.Load("Emojinals/list") as TextAsset;
            if (names != null)
            {
                var filesNames = JsonUtility.FromJson<Data>(names.text);

                foreach (var file in filesNames.Files)
                {
                    var emotion = new Emotions()
                    {
                        Name = file.Name,
                        Description = file.Description,
                        Sprite = Resources.Load<Sprite>("Emojinals/" + file.FileName.Substring(0, file.FileName.Length - 4)) // lose the extension .png to load correctly
                    };
                    EmotionsToTrack.Add(emotion);
                }
            }
        }
    }


}
