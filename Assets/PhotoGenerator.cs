﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PhotoGenerator : MonoBehaviour
{
    public EmotionManager EmotionManager;
    public PhotoPopulate PhotoPopulate;

    public int PhotoCount = 2;
    public bool Regenerate;

    void Update()
    {
        if (Regenerate)
        {
            PhotoPopulate.Clear();
            CreatePhotos();
            Regenerate = false;
        }
    }

    private void CreatePhotos()
    {
        var playerPhotos = new List<TrackedPlayerFaces>();
        for (var i = 0; i < PhotoCount; i++)
        {
            playerPhotos.Add(NewPlayerPhoto(i));
        }
        SetPhotos(playerPhotos);
    }

    private TrackedPlayerFaces NewPlayerPhoto(int index)
    {
        var emotionImage = EmotionManager.GetRandomEmotionImage();
        var player = new TrackedPlayerFaces()
        {
            Name = "Player" + (index + 1),
            Value = UnityEngine.Random.Range(0f, 1f),
            Emotion = emotionImage.Name,
            Sprite = emotionImage.Image
        };

        return player;
    }

    private void CreatePhotos(List<TrackedPlayerFaces> faces)
    {
        SetPhotos(faces);
    }

    private void SetPhotos(List<TrackedPlayerFaces> photos)
    {
        foreach (var photo in photos)
        {
            PhotoPopulate.Create(photo.Emotion, photo.Sprite);
        }
    }

    public IEnumerator GeneratePhotos(UnityAction<Sprite> OnComplete)
    {
        PhotoPopulate.Clear();
        CreatePhotos();

        // Wait for grid layout to work out positioning
        yield return new WaitForSeconds(0.5f);

        var rt = PhotoPopulate.gameObject.GetComponent<RectTransform>();

        // Get Screenshot from texture
        var sprite = GameObject.Find("PhotoCamera").GetComponent<CameraScreenShot>()
            .GetShot(rt);
        if (OnComplete != null)
        {
            yield return null;
            OnComplete(sprite);
        }
    }

    public IEnumerator GeneratePlayerPhotos(List<TrackedPlayerFaces> faces, UnityAction<Sprite> OnComplete)
    {
        PhotoPopulate.Clear();

        CreatePhotos(faces);

        // Wait for grid layout to work out positioning
        yield return new WaitForSeconds(0.5f);

        var rt = PhotoPopulate.gameObject.GetComponent<RectTransform>();

        var sprite = GameObject.Find("PhotoCamera").GetComponent<CameraScreenShot>()
            .GetShot(rt);
        if (OnComplete != null)
        {
            yield return null;
            OnComplete(sprite);
        }
    }
}
