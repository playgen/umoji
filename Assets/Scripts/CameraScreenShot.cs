﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CameraScreenShot : MonoBehaviour
{
    public Image TestImage;
    public GameObject PhotoObject;
    public GameObject PhotoPanel;

    private Dictionary<Sprite, string> _sprites = new Dictionary<Sprite, string>();
    private List<TrackedPlayerFaces> _playerFaces = new List<TrackedPlayerFaces>();
    private Camera _camera;

    private List<GameObject> _createdGameObjects = new List<GameObject>();

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            TakeScreenshot("Test", "player 1", UnityEngine.Random.Range(0f, 1f));
            TestImage.sprite = _sprites.ElementAt(_sprites.Count - 1).Key;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            ShowAllShots();
        }
    }

    public void TakeScreenshot(string emotion, string player, float value)
    {
        var rect = TestImage.GetComponent<RectTransform>();
        var shot = GetShot(rect);

        _playerFaces.Add(new TrackedPlayerFaces()
        {
            Emotion = emotion,
            Name = player,
            Sprite = shot,
            Value = value
        });
    }

    public void ShowAllShots()
    {
        StartCoroutine(ShowShots());
    }

    public IEnumerator WaitShowAllShots()
    {
        yield return ShowShots();
    }

    private IEnumerator ShowShots()
    {
        foreach (var sprite in _sprites)
        {
            var go = Instantiate(PhotoObject);
            go.transform.SetParent(PhotoPanel.transform, true);
            go.GetComponent<PhotoSetup>().Set(sprite.Value, sprite.Key);
            go.GetComponent<Animation>().Play();
            _createdGameObjects.Add(go);
            yield return new WaitForSeconds(go.GetComponent<Animation>().clip.length);
        }
    }

    public TrackedPlayerFaces GetBestRated()
    {
        if (_playerFaces.Count == 0)
        {
            return null;
        }

        var max = _playerFaces[0];

        foreach (var face in _playerFaces)
        {
            if (face.Value > max.Value)
            {
                max = face;
            }
        }
        return max;
    }

    public TrackedPlayerFaces GetLastShot()
    {
        if (_playerFaces.Count == 0)
        {
            return null;
        }
        return _playerFaces.Last();
    }

    public List<TrackedPlayerFaces> GetPlayersBestRated()
    {
        var faces = new List<TrackedPlayerFaces>();

        foreach (var face in _playerFaces)
        {
            Debug.Log(face.Name + " " + face.Emotion + " " + face.Value);
            var containsPlayer = faces.Any(f => f.Name == face.Name);
            if (containsPlayer)
            {
                // check which face has a better value
                var currentStoredFace = faces.Find(f => f.Name == face.Name);
                if (face.Value > currentStoredFace.Value)
                {
                    // replace the stored face
                    faces.Remove(currentStoredFace);
                    faces.Add(face);
                }
            }
            else
            {
                faces.Add(face);
            }
        }
        return faces;
    }

    public List<TrackedPlayerFaces> GetFacesForEmotion(string emotion)
    {
        var faces = new List<TrackedPlayerFaces>();

        foreach (var face in _playerFaces)
        {
            if (face.Emotion == emotion)
            {
                var containsPlayer = faces.Any(f => f.Name == face.Name);
                if (containsPlayer)
                {
                    // check which face has a better value
                    var currentStoredFace = faces.Find(f => f.Name == face.Name);
                    if (face.Value > currentStoredFace.Value)
                    {
                        // replace the stored face
                        faces.Remove(currentStoredFace);
                        faces.Add(face);
                    }
                }
                else
                {
                    faces.Add(face);
                }
            }
        }

        return faces;
    }

    public Texture2D GetShotTexture(RectTransform rt)
    {
        return TakeNewShot(rt.rect.width, rt.rect.height);
    }

    public Sprite GetShot(RectTransform rt)
    {
        var texture = TakeNewShot(rt.rect.width, rt.rect.height);

        return Sprite.Create(texture, new Rect(0, 0, rt.rect.width, rt.rect.height), Vector2.zero);
    }
    private Texture2D TakeNewShot(float width, float height)
    {
        if (_camera == null)
        {
            _camera = GetComponent<Camera>();
        }        

        var rt = new RenderTexture(Mathf.RoundToInt(width), Mathf.RoundToInt(height), 24);
        var screenshot = new Texture2D(Mathf.RoundToInt(width), Mathf.RoundToInt(height), TextureFormat.RGB24, false);
        var activeRT = RenderTexture.active;
        var activeTT = _camera.targetTexture;

        _camera.targetTexture = rt;

        _camera.Render();
        RenderTexture.active = rt;
        screenshot.ReadPixels(new Rect(0,0, width, height), 0, 0);
        _camera.targetTexture = activeTT;
        RenderTexture.active = activeRT;

        screenshot.Apply();

        return screenshot;
    }

    public void ClearPhotos()
    {
        foreach (var createdGameObject in _createdGameObjects)
        {
            Destroy(createdGameObject);
        }
        _createdGameObjects = new List<GameObject>();
        _sprites = new Dictionary<Sprite, string>();
    }

}
