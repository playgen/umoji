﻿using System.Collections;
using System.Collections.Generic;
using EmotionTracking;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public int TimeAvailable;
    public Emotions TargetEmotion;

    private UI_GameManager _uiGameManager;
    private UI_SetupManager _uiSetupManager;

    private List<string> _emotions;

    private List<int> _players;

    private int _currentPlayerIndex;
    private int _round = 1;
    private bool _gameStarted = false;

    void Start()
    {
        _uiGameManager = GameObject.Find("Game").GetComponent<UI_GameManager>();
        _uiSetupManager = GameObject.Find("Setup").GetComponent<UI_SetupManager>();
    }

    public void Setup(int players, int time)
    {
        TimeAvailable = time;

        _players = new List<int>();

        for (var i = 1; i <= players; i++)
        {
            _players.Add(i);
        }

        _uiGameManager.ResetGoal();
    }

    public void ReadyPressed()
    {
        if (!_gameStarted)
        {
            StartPressed();
        }
        _uiGameManager.SetPlayerReady(false);
    }

    private void StartPressed()
    {
        StartGame(GetNewEmotion(), TimeAvailable, _players[_currentPlayerIndex], _round);
        _gameStarted = true;
    }

    private void NextPlayer(int player)
    {
        // Add time available to provide time for switching player
        _uiGameManager.SetPlayerReady(true, player);
        StartGame(GetNewEmotion(), TimeAvailable, player, _round);
    }

    public int CurrentPlayer()
    {
        return _players[_currentPlayerIndex];
    }

    private void StartGame(string targetEmotion, float time, int player, int round)
    {
        _uiGameManager.SetRound(round);
        _uiGameManager.SetStartTime(time);
        _uiGameManager.SetGoal(targetEmotion);
        _uiGameManager.SetPlayer(player);
        _uiGameManager.PromptPlayer(player);
    }

    public void GoalReached()
    {
        _currentPlayerIndex = _currentPlayerIndex + 1 >= _players.Count ? 0 : _currentPlayerIndex + 1;

        if (_currentPlayerIndex == 0)
        {
            TimeAvailable = TimeAvailable > 5 ? TimeAvailable - 1 : TimeAvailable;
            _round += 1;
        }

        if (_players.Count <= 1)
        {
            //_uiGameManager.ShowNotification("Player " + _players[0] + "Won", positive: true);
            _uiGameManager.PauseGame();
            _uiSetupManager.Show();
        }
        else
        {
            NextPlayer(_players[_currentPlayerIndex]);
        }
    }

    public void TimeUp()
    {
        if (_players.Count == 0)
        {
            return;
        }
        _players.RemoveAt(_currentPlayerIndex);

        _currentPlayerIndex = _currentPlayerIndex >= _players.Count ? 0 : _currentPlayerIndex;


        if (_currentPlayerIndex == 0)
        {
            TimeAvailable = TimeAvailable > 5 ? TimeAvailable - 1 : TimeAvailable;
            _round += 1;
        }


        if (_players.Count <= 1)
        {
            //_uiGameManager.ShowNotification("Player " + _players[0] + "Won", positive:true );
           
            var action = new UnityAction(GameOver);
            StartCoroutine(_uiGameManager.ShowPlayer("Player " + _players[0] + " Wins", action));
        }
        else
        {
            NextPlayer(_players[_currentPlayerIndex]);
        }
    }

    private void GameOver()
    {
        _uiGameManager.ShowEndGame();
        //StartCoroutine(BackToMenu());
    }

    public void BackToMenu()
    {
        GameObject.Find("ScreenShotCamera").GetComponent<CameraScreenShot>().ClearPhotos();
        _uiGameManager.HideEndGame();
        _uiSetupManager.Show();
        _uiGameManager.PauseGame();
    }

    private string GetNewEmotion()
    {
        return "";
        // FIXME
        // Get the list of emotions to track from the Affectiva Camera
        //var detector = Camera.main.GetComponent<Detector>();
        //var emotions = detector.emotions;

        //var sewa = Camera.main.GetComponent<SewaEmotions>();
        //var sewaEmotions = sewa.EmotionsToUse;

        //if (_emotions == null || _emotions.Count == 0)
        //{
        //    if (VersionManager.Instance.IsVersion(VersionManager.Version.Affectiva))
        //    {
        //        _emotions = new List<string>();

        //        for (int i = 0; i < System.Enum.GetNames(typeof(Emotions)).Length; i++)
        //        {
        //            Emotions targetEmotion = (Emotions) i;
        //            if (emotions.On(targetEmotion))
        //            {
        //                _emotions.Add(targetEmotion.ToString());
        //            }
        //        }

        //    }
        //    else
        //    {
        //        _emotions = new List<string>();

        //        for (int i = 0; i < System.Enum.GetNames(typeof(EmotionsList)).Length; i++)
        //        {
        //            EmotionsList targetEmotion = (EmotionsList) i;
        //            if (sewaEmotions.On(targetEmotion))
        //            {
        //                _emotions.Add(targetEmotion.ToString());
        //            }
        //        }
        //    }
        //}
        //// return a random emotion from the list
        //var random = Random.Range(0, _emotions.Count);

        //return _emotions[random];
    }
}
