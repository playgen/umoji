﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PlayerEmotions : MonoBehaviour
{

    public GameObject Error;

    private Dictionary<string, float> _currentEmotions;

    // The amount of time the face is lost for
    private float _faceLostTimer;
    
    // The amount of time the face is lost for before showing error
    private readonly float _faceLostErrorLimit = 1.00f;

    private bool _foundFace;


    void Start()
    {
        if (Error != null)
        {
            // Additional code for tracking if face is found
            Error.SetActive(false);
        }
        _currentEmotions = new Dictionary<string, float>();


        // FIXME
        // Get a new list of emotions that the game supports

        //// Get the list of emotions to track from the Affectiva Camera
        //var detector = Camera.main.GetComponent<Detector>();
        //var emotions = detector.emotions;

        //if (VersionManager.Instance != null)
        //{

        //    var sewa = Camera.main.GetComponent<SewaEmotions>();
        //    if (sewa != null)
        //    {
        //        if (VersionManager.Instance.IsVersion(VersionManager.Version.SEWA))
        //        {
        //            var sewaEmotions = sewa.EmotionsToUse;

        //            for (int i = 0; i < System.Enum.GetNames(typeof(EmotionsList)).Length; i++)
        //            {
        //                EmotionsList targetEmotion = (EmotionsList) i;
        //                if (sewaEmotions.On(targetEmotion))
        //                {
        //                    _currentEmotions.Add(targetEmotion.ToString(), 0f);
        //                }
        //            }
        //        }
        //    }

        //    if (VersionManager.Instance.IsVersion(VersionManager.Version.Affectiva))
        //    {
        //        for (int i = 0; i < System.Enum.GetNames(typeof(Emotions)).Length; i++)
        //        {
        //            Emotions targetEmotion = (Emotions) i;
        //            if (emotions.On(targetEmotion))
        //            {
        //                _currentEmotions.Add(targetEmotion.ToString(), 0f);
        //            }
        //        }
        //    }
        //}
        //else
        //{
        //    // Not in main game Use affectiva by default
        //    for (int i = 0; i < System.Enum.GetNames(typeof(Emotions)).Length; i++)
        //    {
        //        Emotions targetEmotion = (Emotions)i;
        //        if (emotions.On(targetEmotion))
        //        {
        //            _currentEmotions.Add(targetEmotion.ToString(), 0f);
        //        }
        //    }
        //}
    }

    void Update()
    {
        if (_foundFace)
        {
            _faceLostTimer = 0f;
            if (Error != null && Error.activeSelf)
            {
                Error.SetActive(false);
            }
        }
        else
        {
            if (Error != null && !Error.activeSelf)
            {
                _faceLostTimer += Time.deltaTime;
                if (_faceLostTimer >= _faceLostErrorLimit)
                {
                    Error.SetActive(true);
                }
            }
        }
    }

    public void FaceFound()
    {
        _foundFace = true;
    }

    public void FaceLost()
    {
        _foundFace = false;
    }

    // FIXME 
    // No longer using affectiva, but should use same overrides later

    //public override void onFaceFound(float timestamp, int faceId)
    //{
    //    _foundFace = true;
    //}

    //public override void onFaceLost(float timestamp, int faceId)
    //{
    //    _foundFace = false;
    //}

    //public override void onImageResults(Dictionary<int, Face> faces)
    //{
    //    //Debug.Log("Got face results");

    //    foreach (KeyValuePair<int, Face> pair in faces)
    //    {
    //        int FaceId = pair.Key;  // The Face Unique Id.
    //        Face face = pair.Value;    // Instance of the face class containing emotions, and facial expression values.

    //        //Retrieve the Emotions Scores]
    //        for (int i = 0; i < _currentEmotions.Count; i++)
    //        {
    //            var emotion = _currentEmotions.ElementAt(i).Key;
    //            var value = 0f;

    //            face.Emotions.TryGetValue(emotion, out value);

    //            _currentEmotions[emotion] = value;
    //        }
    //    }
    //}

    public float GetValue(string emotion)
    {
        return _currentEmotions[emotion];
    }

    public bool HasFace()
    {
        return _foundFace;
    }
}