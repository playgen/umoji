﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotoSetup : MonoBehaviour
{
    [SerializeField] private float _maxZRotation;
    [SerializeField] private float _minZRotation;

    public Text Title;
    public Image Image;

    public void Set(string emotion, Sprite sprite)
    {
        var zRot = UnityEngine.Random.Range(_minZRotation, _maxZRotation);

        GetComponent<RectTransform>().rotation = Quaternion.Euler(0f, 0f, zRot);

        Title.text = emotion;
        Image.sprite = sprite;
    }
}
