﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ProgressBar : MonoBehaviour
{
    [SerializeField] private RectTransform _progressBar;
    [SerializeField] private Image _progressBarFill;
    [SerializeField] private RectTransform _progressBarHighlight;

    [SerializeField] private RectTransform _star1;
    [SerializeField] private RectTransform _star2;
    [SerializeField] private RectTransform _star3;

    [SerializeField] private float _goal1 = .25f;
    [SerializeField] private float _goal2 = .6f;
    [SerializeField] private float _goal3 = 1f;

    public Sprite DefaultStar;
    public Sprite ReachedStar;

    [Range(0f, 1f)] [SerializeField] private float _value;

    void Start()
    {
        Setup(_goal1, _goal2, _goal3);
    }

    public void Setup(float goal1, float goal2, float goal3)
    {
        _goal1 = goal1;
        _goal2 = goal2;
        _goal3 = goal3;

        SetStars();
        SetHighlight();
    }

    private void SetStars()
    {
        var anchorX = _progressBar.anchorMin.x;
        var barWidth = _progressBar.rect.width;

        _star1.anchorMin = new Vector2(anchorX, _star1.anchorMin.y);
        _star1.anchorMax = new Vector2(anchorX, _star1.anchorMax.y);
        _star1.anchoredPosition = new Vector2(barWidth * _goal1, 0f);

        _star2.anchorMin = new Vector2(anchorX, _star2.anchorMin.y);
        _star2.anchorMax = new Vector2(anchorX, _star2.anchorMax.y);
        _star2.anchoredPosition = new Vector2(barWidth * _goal2, 0f);

        _star3.anchorMin = new Vector2(anchorX, _star3.anchorMin.y);
        _star3.anchorMax = new Vector2(anchorX, _star3.anchorMax.y);
        _star3.anchoredPosition = new Vector2(barWidth * _goal3, 0f);
    }

    private void SetHighlight()
    {
        var anchorX = _progressBar.anchorMin.x;
        var barWidth = _progressBar.rect.width;

        _progressBarHighlight.anchorMin = new Vector2(anchorX, _progressBarHighlight.anchorMin.y);
        _progressBarHighlight.anchorMax = new Vector2(anchorX, _progressBarHighlight.anchorMax.y);
        _progressBarHighlight.anchoredPosition = new Vector2(barWidth * _progressBarFill.fillAmount, 0f);
    }

    public void UpdateProgress(float value)
    {
        _value = value;
        UpdateProgressBar();
        SetHighlight();
        UpdateStarImage();
    }

    private void UpdateProgressBar()
    {
        _progressBarFill.fillAmount = _value;
    }

    private void UpdateStarImage()
    {
        _star1.GetComponent<Image>().sprite = _value >= _goal1 ? ReachedStar : DefaultStar;
        _star2.GetComponent<Image>().sprite = _value >= _goal2 ? ReachedStar : DefaultStar;
        _star3.GetComponent<Image>().sprite = _value >= _goal3 ? ReachedStar : DefaultStar;
    }

    void Update()
    {
        UpdateProgress(_value);
    }
}
