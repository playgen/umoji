﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UI_GameManager : MonoBehaviour
{
    public GameObject AffectivaBody;
    public GameObject SewaBody;

    public GameObject EndGameObject;

    public bool UpdateUI { get; set; }
    public bool RachedGaol { get; set; }

    private float _time;
    private string _goalEmotion;

    private UI_EmotionStatus _uiEmotionStatus;
    private UI_Goal _uiGoal;
    private UI_Notification _uiNotification;
    private UI_GameStart _uiGameStart;
    private UI_EndGameManager _uiEndGameManager;

    private GameManager _gameManager;
    private EmotionManager _emotionManager;

    private Sprite _emotionSprite;

    private string _playerName;


    void Start()
    {
        AffectivaBody.SetActive(VersionManager.Instance.IsVersion(VersionManager.Version.Custom));

        _uiEmotionStatus = GameObject.Find("StatusContainer").GetComponent<UI_EmotionStatus>();
        _uiGoal = GameObject.Find("InfoContainer").GetComponent<UI_Goal>();
        _uiNotification = GameObject.Find("NotificationContainer").GetComponent<UI_Notification>();
        _uiGameStart = GameObject.Find("ReadyContainer").GetComponent<UI_GameStart>();
        _uiEndGameManager = EndGameObject.GetComponent<UI_EndGameManager>();

        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _emotionManager = Camera.main.GetComponent<EmotionManager>();

        SetPlayerReady(true, 1);
    }

    public void ResetGoal()
    {
        _uiGoal.Reset();
    }

    public void PromptPlayer(int player)
    {
        _uiEmotionStatus.SetGoalAmount();
        RachedGaol = false;
        StartGame();
    }

    public void StartGame()
    {
        UpdateUI = true;
    }

    public void PauseGame()
    {
        UpdateUI = false;
    }

    public void SetRound(int round)
    {
        _uiGoal.SetRound(round);
    }

    public void SetStartTime(float time)
    {
        _time = time;

        _uiGoal.SetStartTime(Mathf.RoundToInt(_time));
    }

    public void SetPlayerReady(bool active, int player = 0)
    {
        if (player != 0)
        {
            _playerName = "Player " + player;
        }
        if (active)
        {
            _uiGameStart.Show(_playerName);
        }
        else
        {
            _uiGameStart.Hide();
        }
    }

    public bool AwaitingPlayerReady()
    {
        return _uiGameStart.IsShowing();
    }

    public void SetPlayer(int player)
    {
        _uiGoal.SetPlayer(player);
    }

    public void SetGoal(string goal)
    {
        _goalEmotion = goal;
        _emotionSprite = _emotionManager.GetEmotionSprite(goal);
        _uiGoal.SetEmotion(_emotionSprite);
    }

    public void ReachedTarget(string emotion, float value)
    {
        if (emotion == _goalEmotion && !RachedGaol && UpdateUI)
        {
            RachedGaol = true;
            GameObject.Find("ScreenShotCamera").GetComponent<CameraScreenShot>().TakeScreenshot(emotion, _playerName, value);

            // Level complete
            var action = new UnityAction(_gameManager.GoalReached);
            StartCoroutine(ShowNotification("Good Job", true, action));
        }
    }

    void Update()
    {
        if (UpdateUI)
        {
            if (!AwaitingPlayerReady())
            {
                _uiEmotionStatus.Tick(this, _goalEmotion);
                _time -= Time.deltaTime;
            }

            _uiGoal.SetTime(Mathf.RoundToInt(_time));

            if (_time <= 0f)
            {
                UpdateUI = false;
                var action = new UnityAction(_gameManager.TimeUp);
                StartCoroutine(ShowPlayer("Eliminated", action));
                //StartCoroutine(ShowNotification("Time Up", false, action));
            }
        }
    }

    public IEnumerator ShowNotification(string text, bool positive, UnityAction action)
    {
        yield return _uiNotification.ShowNotification(text, positive);

        action();
    }

    public IEnumerator ShowPlayer(string text, UnityAction action)
    {
        yield return _uiNotification.ShowPlayer(text);
           
        if (action != null)
        {
            action();
        }
    }

    public void HideEndGame()
    {
        _uiEndGameManager.Hide();
    }

    public void ShowEndGame()
    {
        _uiEndGameManager.Show();

        var camScreenShot = GameObject.Find("ScreenShotCamera").GetComponent<CameraScreenShot>();

        var winner = camScreenShot.GetLastShot().Sprite;
        var best = camScreenShot.GetBestRated().Sprite;

        _uiEndGameManager.ShowEndGame(winner, best, camScreenShot);
    }
}
