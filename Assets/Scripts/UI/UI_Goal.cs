﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Goal : MonoBehaviour
{
    public Text Player;
    public Image Emotion;
    public Image Timer;

    private int _round;
    private int _startTime;
    private int _timeRemaining;

    public void Reset()
    {
        Player.text = "";

        _round = 1;
    }

    public void SetRound(int round)
    {
        _round = round;
    }

    public void SetEmotion(Sprite emotion)
    {
        Emotion.sprite = emotion;
    }

    public void SetPlayer(int playerNum)
    {
        Player.text = "Player " + playerNum + " Round " + _round;
    }

    public void SetStartTime(int time)
    {
        _startTime = time;
        _timeRemaining = _startTime;
        UpdateClockDisplay();
    }
    public void SetTime(int time)
    {
        if (time <= 0)
        {
            time = 0;
        }
        _timeRemaining = time;
        UpdateClockDisplay();
    }

    private void UpdateClockDisplay()
    {
        var timeTaken = (float) _timeRemaining / (float) _startTime;
        Timer.fillAmount = timeTaken;
    }
}
