﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_EmotionDisplay : MonoBehaviour
{

    public Text Emotion;
    public Image StatusBar;

    public Color Normal;
    public Color Target;

    public void SetEmotion(string emotion)
    {
        Emotion.text = emotion;
    }

    /// <summary>
    /// Set the status of the emotion
    /// </summary>
    /// <param name="value">value range 0-1</param>
    public void SetStatusValue(float value, bool istarget)
    {
        StatusBar.fillAmount = value;

        Emotion.color = istarget ? Target : Normal;
        StatusBar.color = Emotion.color;
    }
	
}
