﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Notification : MonoBehaviour
{

    public Text NotificationText;
    public Text PlayerTurnText;

    public Color PositiveColor;
    public Color NegativeColor;

    private Animation _animationNotification;
    private Animation _animationPlayer;

    void Start()
    {
        _animationNotification = NotificationText.GetComponent<Animation>();
        _animationPlayer = PlayerTurnText.GetComponent<Animation>();

        NotificationText.transform.localScale = Vector3.zero;
        PlayerTurnText.transform.localScale = Vector3.zero;
    }
    public IEnumerator ShowNotification(string msg, bool positive)
    {
        while (_animationNotification.isPlaying)
        {
            yield return null;
        }

        NotificationText.color = positive ? PositiveColor : NegativeColor;
        NotificationText.text = msg;

        _animationNotification.Play();

        yield return new WaitForSeconds(_animationNotification.clip.length);
    }

    public IEnumerator ShowPlayer(string msg)
    {
        while (_animationPlayer.isPlaying)
        {
            yield return null;
        }

        PlayerTurnText.text = msg;

        _animationPlayer.Play();

        yield return new WaitForSeconds(_animationPlayer.clip.length);

    }
}
