﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_GameStart : MonoBehaviour
{
    private UI_GameManager _uiGameManager;

    private CanvasGroup _canvasGroup;

    public Text PlayerText;

    void Start()
    {
        _uiGameManager = GameObject.Find("Game").GetComponent<UI_GameManager>();
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    public void Hide()
    {
        _canvasGroup.alpha = 0f;
        _canvasGroup.interactable = false;
    }

    public void Show(string playerText)
    {
        _canvasGroup.alpha = 1f;
        _canvasGroup.interactable = true;
        PlayerText.text = playerText;
    }

    public bool IsShowing()
    {
        return _canvasGroup.alpha == 1f;
    }

}
