﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_EmotionStatus : MonoBehaviour
{
    public GameObject EmotionDisplay;

    private List<string> _emotionsToTrack;

    private Dictionary<string, UI_EmotionDisplay> _emotionsDisplays;

    private float _goal = 1f;

    public GameObject Content;

    private PlayerEmotions _playerEmotions;

    [SerializeField] private UI_ProgressBar _progressBar;
    /// <summary>
    /// Set the required goal amount
    /// </summary>
    /// <param name="goal">goal value 0-1</param>
    public void SetGoalAmount(float goal = 0.85f)
    {
        _goal = goal;
    }

    void Start()
    {
        _playerEmotions = GetComponent<PlayerEmotions>();

        //_playerEmotions.enabled = VersionManager.Instance.IsVersion(VersionManager.Version.Affectiva);

        SetTrackedEmotions();
    }

    public void SetTrackedEmotions()
    {
        // FIXME

        // Get the list of emotions to track from the Affectiva Camera
        //var detector = Camera.main.GetComponent<Detector>();
        //var emotions = detector.emotions;

        //_emotionsToTrack = new List<string>();

        //if (VersionManager.Instance.IsVersion(VersionManager.Version.Custom))
        //{
        //    for (int i = 0; i < System.Enum.GetNames(typeof(Emotions)).Length; i++)
        //    {
        //        Emotions targetEmotion = (Emotions) i;
        //        if (emotions.On(targetEmotion))
        //        {
        //            _emotionsToTrack.Add(targetEmotion.ToString());
        //        }
        //    }
        //}
        //else
        //{
        //    for (int i = 0; i < System.Enum.GetNames(typeof(EmotionsList)).Length; i++)
        //    {
        //        EmotionsList targetEmotion = (EmotionsList)i;
        //        if (sewaEmotions.On(targetEmotion))
        //        {
        //            _emotionsToTrack.Add(targetEmotion.ToString());
        //        }
        //    }
        //}

        //// Generate the status bars in the content
        //ClearChildren(Content);

        //_emotionsDisplays = new Dictionary<string, UI_EmotionDisplay>();

        //foreach (var emotion in _emotionsToTrack)
        //{
        //    var go = Instantiate(EmotionDisplay);
        //    go.transform.SetParent(Content.transform);
            
        //    var display = go.GetComponent<UI_EmotionDisplay>();
        //    display.SetEmotion(emotion);
        //    display.SetStatusValue(0f, false);

        //    _emotionsDisplays.Add(emotion, display);
        //}
    }

    public void Tick(UI_GameManager manager, string emotion)
    {
        if (_emotionsDisplays != null && _emotionsDisplays.Count > 0)
        {
            // We need to track changes
            foreach (var uiEmotionDisplay in _emotionsDisplays)
            {
                var fill = 0f;
                if (VersionManager.Instance.IsVersion(VersionManager.Version.Custom))
                {
                    // Get Value from face
                    fill = _playerEmotions.GetValue(uiEmotionDisplay.Key) / 100f;
                }

                var isTarget = uiEmotionDisplay.Key == emotion;
                if (isTarget)
                {
                    _progressBar.UpdateProgress(fill);
                }

                //uiEmotionDisplay.Value.SetStatusValue(fill, isTarget);

                if (fill >= _goal)
                {
                    // Goal Reached
                    manager.ReachedTarget(uiEmotionDisplay.Key, fill);
                }
            }
        }
    }

    private void ClearChildren(GameObject parent)
    {
        foreach (Transform transform1 in parent.transform)
        {
            Destroy(transform1.gameObject);
        }
    }
}
