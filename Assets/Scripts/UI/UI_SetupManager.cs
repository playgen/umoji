﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_SetupManager : MonoBehaviour
{
    public GameObject AffectivaName;
    public GameObject SewaName;

    public Text Players;
    //public InputField Time;

    private GameManager _gameManager;
    private CanvasGroup _canvasGroup;

    [SerializeField] private int _minPlayers = 2;
    [SerializeField] private int _maxPlayers = 8;

    private int _players = 2;
    private int _time = 10;

    void Start()
    {
        if (AffectivaName != null)
        {
            AffectivaName.SetActive(VersionManager.Instance.IsVersion(VersionManager.Version.Custom));
        }

        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _canvasGroup = GetComponent<CanvasGroup>();

        Players.text = _players + " Players";

        Show();
    }

    public void Show()
    {
        _canvasGroup.alpha = 1f;
        _canvasGroup.interactable = true;
        _canvasGroup.blocksRaycasts = true;
    }

    public void Hide()
    {
        _canvasGroup.alpha = 0f;
        _canvasGroup.interactable = false;
        _canvasGroup.blocksRaycasts = false;
    }

    public void ReadyPressed()
    {
        //var time = Convert.ToInt16(Time.text);
        _gameManager.Setup(_players, _time);
        Hide();
    }

    public void OnPlayersSet()
    {
        var players = Convert.ToInt16(Players.text);
        if (players <= 1)
        {
            Players.text = "2";
        }
    }

    //public void OnTimeSet()
    //{
    //    var time = Convert.ToInt16(Time.text);
    //    if (time <= 5)
    //    {
    //        Time.text = "5";
    //    }
    //}

    public void ChangePlayerCount(int increment)
    {
        _players += increment;
        _players = Mathf.Clamp(_players, _minPlayers, _maxPlayers);
        Players.text = _players + " Players";
    }
}
