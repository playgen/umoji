﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Photo : MonoBehaviour
{

    public Image PlayerPhoto;
    public Image EmotionImage;

    public void Set(Sprite player, Sprite emotion)
    {
        PlayerPhoto.sprite = player;
        EmotionImage.sprite = emotion;
    }
}
