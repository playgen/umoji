﻿using UnityEngine;

public class TrackedPlayerFaces {
    public string Name;
    public string Emotion;
    public Sprite Sprite;
    public float Value;
}
