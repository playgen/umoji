﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VersionManager : MonoBehaviour
{
    public enum Version
    {
        Custom,
    }

    public Version VersionToUse;

    public static VersionManager Instance;

    void Awake()
    {
        Instance = this;
    }

    public bool IsVersion(Version version)
    {
        return VersionToUse == version;
    }
    
}
