﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoPopulate : MonoBehaviour
{

    public GameObject PhotoGameObject;
    private EmotionManager _emotionManager;


    public void Create(string emotion, Sprite playerSprite)
    {
        if (_emotionManager == null)
        {
            _emotionManager = Camera.main.GetComponent<EmotionManager>();
        }

        var go = Instantiate(PhotoGameObject);
        go.transform.SetParent(this.transform, true);
        go.transform.localScale = Vector3.one;
        go.GetComponent<UI_Photo>().Set(playerSprite, _emotionManager.GetEmotionSprite(emotion));
    }

    public void Clear()
    {
        var childPhotos = GetComponentsInChildren<UI_Photo>();
        foreach (UI_Photo photo in childPhotos)
        {
            Destroy(photo.gameObject);
        }
    }
}
