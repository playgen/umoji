﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_EndGameManager : MonoBehaviour
{

    public Sprite Test1, Test2;
    private GameManager _gameManager;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ShowEndGame(Test1, Test2, null);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Setup();
        }
    }

    [SerializeField] private Animation _flareAnimation;
    [SerializeField] private Animation _victoryAnimation;
    [SerializeField] private Animation _bestFaceAnimation;
    [SerializeField] private GameObject _menuButton;
    [SerializeField] private GameObject _collagePanel;
    [SerializeField] private GameObject _collageElement;

    private string _flareScaleUp = "ScaleUp";
    private string _flareScaleDown = "ScaleDown";
    
    private string _showAnimName = "EndGameFaceShow";
    private string _moveLeftAnimName = "EndGameMoveLeft";
    private string _moveRightAnimName = "EndGameMoveRight";

    private CanvasGroup _canvasGroup;

    void Start()
    {
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        Setup();

        _canvasGroup = GetComponent<CanvasGroup>();
        Hide();
    }

    public void Show()
    {
        _canvasGroup.alpha = 1f;
        _canvasGroup.interactable = true;
        _canvasGroup.blocksRaycasts = true;
    }

    public void Hide()
    {
        _canvasGroup.alpha = 0f;
        _canvasGroup.interactable = false;
        _canvasGroup.blocksRaycasts = false;
    }

    private void Setup()
    {
        _flareAnimation.gameObject.SetActive(false);
        _victoryAnimation.gameObject.SetActive(false);
        _bestFaceAnimation.gameObject.SetActive(false);
        _menuButton.SetActive(false);
        _collagePanel.SetActive(false);
    }

    public void ShowEndGame(Sprite winningPlayer, Sprite bestPlayer, CameraScreenShot camSS)
    {
        if (camSS != null)
        {
            GeneratePlayerImages(camSS);
        }
        else
        {
            // Pre load the images for the end of game
            StartCoroutine(GenerateImages(5));
        }
        // Start the end game animations
        StartCoroutine(PlayAnimations(winningPlayer, bestPlayer));
    }

    private IEnumerator PlayAnimations(Sprite winningPlayer, Sprite bestPlayer)
    {
        _flareAnimation.gameObject.SetActive(true);
        _flareAnimation.Play(_flareScaleUp);
        yield return new WaitForSeconds(_flareAnimation[_flareScaleUp].length);

        _victoryAnimation.gameObject.SetActive(true);
        _victoryAnimation.transform.Find("Panel/PlayerPhoto").GetComponent<Image>().sprite = winningPlayer;
        _victoryAnimation.Play(_showAnimName);
        yield return new WaitForSeconds(_victoryAnimation[_showAnimName].length);

        _victoryAnimation.Play(_moveLeftAnimName);
        yield return new WaitForSeconds(_victoryAnimation[_moveLeftAnimName].length);

        _bestFaceAnimation.gameObject.SetActive(true);
        _bestFaceAnimation.transform.Find("Panel/PlayerPhoto").GetComponent<Image>().sprite = bestPlayer;
        _bestFaceAnimation.Play(_showAnimName);
        yield return new WaitForSeconds(_victoryAnimation[_showAnimName].length);

        _bestFaceAnimation.Play(_moveRightAnimName);
        yield return new WaitForSeconds(_victoryAnimation[_moveRightAnimName].length);

        _flareAnimation.Play(_flareScaleDown);
        yield return new WaitForSeconds(_flareAnimation[_flareScaleDown].length);
        _flareAnimation.gameObject.SetActive(false);

        _menuButton.SetActive(true);
        _collagePanel.SetActive(true);
        StartCoroutine(UpdateContentSize(_collagePanel.GetComponentInChildren<ScrollRect>().content.gameObject));
    }

    private IEnumerator GenerateImages(int count)
    {
        var content = _collagePanel.GetComponentInChildren<ScrollRect>().content;

        for (var i = 0; i < count; i++)
        {
            var go = Instantiate(_collageElement);
            go.transform.SetParent(content.transform);
            go.transform.localScale = Vector3.one;
            yield return go.GetComponentInChildren<PhotoCreate>().Setup();
        }
    }

    private void GeneratePlayerImages(CameraScreenShot cameraScreenShot)
    {
        // get best photos for each player
        StartCoroutine(GenerateCollage(cameraScreenShot.GetPlayersBestRated()));
        // get best photos for each emotion category
        
    }

    private IEnumerator GenerateCollage(List<TrackedPlayerFaces> playerFaces)
    {
        if (playerFaces != null)
        {
            var content = _collagePanel.GetComponentInChildren<ScrollRect>().content;
            var go = Instantiate(_collageElement);
            go.transform.SetParent(content.transform);
            go.transform.localScale = Vector3.one;
            yield return go.GetComponentInChildren<PhotoCreate>().SetupWithPlayerPhotos(playerFaces);
        }
    }

    private IEnumerator UpdateContentSize(GameObject content)
    {
        yield return new WaitForEndOfFrame();

        content.GetComponent<SetCellSize>().Set();

        var grid = content.GetComponent<GridLayoutGroup>();
        var rect = content.GetComponent<RectTransform>();

        var height = (content.transform.childCount / grid.constraintCount) + 1; 

        var newHeight = (grid.cellSize.y * height) + (grid.spacing.y * height);

        rect.sizeDelta = new Vector2(0, newHeight);
    }

    public void BackToMenu()
    {
        _gameManager.BackToMenu();
    }
}



