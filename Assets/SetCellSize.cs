﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class SetCellSize : MonoBehaviour
{
    public bool Auto = true;
    public bool UseHeight;
    public bool UseWidth;

    private RectTransform _rect;
    private GridLayoutGroup _grid;
	// Use this for initialization
	void Start () {
	    if (_grid == null)
	    {
	        _grid = GetComponent<GridLayoutGroup>();
	    }
	    if (_rect == null)
	    {
	        _rect = GetComponent<RectTransform>();
	    }
	}

    void Update()
    {
        if (Auto)
        {
            Set();
        }
    }

    public void Set()
    { 

        if (_rect == null)
        {
            _rect = GetComponent<RectTransform>();
        }
        if (_grid == null)
        {
            _grid = GetComponent<GridLayoutGroup>();
        }

        var width = _rect.rect.width;
        var height = _rect.rect.height;
	    var constraint = _grid.constraintCount;

	    var cellsDown = 1;//(transform.childCount / constraint) + (transform.childCount % constraint > 0 ? 1 : 0);
	    var cellsAcross = 1;// transform.childCount <= constraint ? transform.childCount : constraint;

	    if (_grid.constraint == GridLayoutGroup.Constraint.FixedColumnCount)
	    {
	        // Vertical
	        cellsAcross = Mathf.Min(transform.childCount, constraint);
            cellsDown = (transform.childCount / constraint) + (transform.childCount % constraint > 0 ? 1 : 0);
        }
        else if (_grid.constraint == GridLayoutGroup.Constraint.FixedRowCount)
	    {
            // Horizontal
	        cellsDown = Mathf.Min(transform.childCount, constraint);
	        cellsAcross = (transform.childCount / constraint) + (transform.childCount % constraint > 0 ? 1 : 0);
        }


        var cellHeight = (height / cellsDown) - _grid.spacing.y;
	    var cellWidth = (width / cellsAcross) - _grid.spacing.x;

        var newCellSize = Mathf.Min(cellWidth, cellHeight);

	    if (UseWidth)
	    {
	        newCellSize = cellWidth - _grid.spacing.x;
	    }
	    if (UseHeight)
	    {
	        newCellSize = cellHeight - _grid.spacing.y; 
	    }

        _grid.cellSize = new Vector2(newCellSize, newCellSize);
	}
}
