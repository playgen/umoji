﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColourRandom : MonoBehaviour
{

    private Image _image;
    private float _timer = 0f;


	// Use this for initialization
	void Start ()
	{
	    _image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update ()
	{

	    _timer += Time.deltaTime;
	    if (_timer > 1.0f)
	    {
	        _timer = 0f;
            _image.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
	    }
	}
}
