﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_CollagePanel : MonoBehaviour
{

    [SerializeField] private GameObject _sharePanel;
    [SerializeField] private Image _shareImage;

    private Sprite _spriteToShare;

    void Start()
    {
        _sharePanel.SetActive(false);
    }

    public void ShowSharePanel(Sprite sprite)
    {
        _spriteToShare = sprite;
        _shareImage.sprite = sprite;
        _shareImage.preserveAspect = true;
        _sharePanel.SetActive(true);
    }

    public void HideSharePanel()
    {
        _sharePanel.SetActive(false);
    }

    public void Share()
    {
        
    }
}
