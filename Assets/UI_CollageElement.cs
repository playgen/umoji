﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_CollageElement : MonoBehaviour
{

    public Image Image;

    public void OnPress()
    {
        GameObject.Find("CollagePanel").GetComponent<UI_CollagePanel>().ShowSharePanel(Image.sprite);
    }
}

