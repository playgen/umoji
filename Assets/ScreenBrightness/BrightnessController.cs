﻿using UnityEngine;
using UnityEngine.UI;

public class BrightnessController : MonoBehaviour
{
	// Update is called once per frame
	void Start () 
    {
        UpdateBrightness();
	}

    public void Set()
    {
        if (!BrightnessLevelPlugin.HasBrightnessAccess())
        {
            UpdateBrightness();
        }
    }
    void FixedUpdate()
    { 
        if (BrightnessLevelPlugin.HasBrightnessAccess() && BrightnessLevelPlugin.GetBrightness() != 255)
        {
            UpdateBrightness();
        }
    }
    
    void UpdateBrightness()
    {
        BrightnessLevelPlugin.SetBrightness();
    }
}
