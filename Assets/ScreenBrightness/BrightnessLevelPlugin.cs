﻿using UnityEngine;

public class BrightnessLevelPlugin 
{
    public BrightnessLevelPlugin()
    {
        
    }

    public static void SetBrightness()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            using (var javaUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                using (var currentActivity = javaUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
                {
                    using (var androidPlugin = new AndroidJavaObject("com.PlayGen.MyProject.AndroidBrightness", currentActivity))
                    {
                        androidPlugin.Call("SetBrightness");
                    }
                }
            }
        }
    }
    public static bool HasBrightnessAccess()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            using (var javaUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                using (var currentActivity = javaUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
                {
                    using (var androidPlugin = new AndroidJavaObject("com.PlayGen.MyProject.AndroidBrightness", currentActivity))
                    {
                        return androidPlugin.Call<bool>("HasBrightnessAccess");
                    }
                }
            }
        }
        return false;
    }
    public static int GetBrightness()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            using (var javaUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                using (var currentActivity = javaUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
                {
                    using (var androidPlugin = new AndroidJavaObject("com.PlayGen.MyProject.AndroidBrightness", currentActivity))
                    {
                        return androidPlugin.Call<int>("GetBrightness");
                    }
                }
            }
        }
        return 255;
    }
}
