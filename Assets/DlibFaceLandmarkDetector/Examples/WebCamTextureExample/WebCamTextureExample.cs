﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif
using DlibFaceLandmarkDetector;
using JetBrains.Annotations;
using UnityEngine.UI;

namespace DlibFaceLandmarkDetectorExample
{
    /// <summary>
    /// WebCamTexture example.
    /// An example of detecting face landmarks in WebCamTexture images.
    /// </summary>
    public class WebCamTextureExample : MonoBehaviour
    {
        /// <summary>
        /// Should use front camera.
        /// </summary>
        public bool shouldUseFrontCamera = false;

        /// <summary>
        /// The webcam texture.
        /// </summary>
        WebCamTexture webCamTexture;
        
        /// <summary>
        /// The webcam device.
        /// </summary>
        WebCamDevice webCamDevice;
        
        /// <summary>
        /// The colors.
        /// </summary>
        Color32[] colors;
        
        /// <summary>
        /// The width.
        /// </summary>
        int width = 320;
        
        /// <summary>
        /// The height.
        /// </summary>
        int height = 240;

        /// <summary>
        /// Indicates whether this instance has been initialized.
        /// </summary>
        bool hasInitDone = false;
        
        /// <summary>
        /// The screenOrientation.
        /// </summary>
        ScreenOrientation screenOrientation = ScreenOrientation.Unknown;

        /// <summary>
        /// The face landmark detector.
        /// </summary>
        FaceLandmarkDetector faceLandmarkDetector;

        /// <summary>
        /// The texture2D.
        /// </summary>
        Texture2D texture2D;

        /// <summary>
        /// Indicates whether the image is flipped
        /// </summary>
        bool flip;

        /// <summary>
        /// The shape_predictor_68_face_landmarks_dat_filepath.
        /// </summary>
        string shape_predictor_68_face_landmarks_dat_filepath;

        public RawImage CameraFeed;

        // Slice rect used to transition and remove jittering in texture update
        private Rect _sliceRect;
        private Texture2D _texture2D;


#if UNITY_WEBGL && !UNITY_EDITOR
        Stack<IEnumerator> coroutines = new Stack<IEnumerator> ();
#endif

        // Use this for initialization
        void Start ()
        {
            #if UNITY_WEBGL && !UNITY_EDITOR
            var getFilePath_Coroutine = Utils.getFilePathAsync ("shape_predictor_68_face_landmarks.dat", (result) => {
                coroutines.Clear ();

                shape_predictor_68_face_landmarks_dat_filepath = result;
                Run ();
            });
            coroutines.Push (getFilePath_Coroutine);
            StartCoroutine (getFilePath_Coroutine);
            #else
            //shape_predictor_68_face_landmarks_dat_filepath = Utils.getFilePath ("shape_predictor_68_face_landmarks.dat");
            shape_predictor_68_face_landmarks_dat_filepath = Utils.getFilePath ("shape_predictor_68_face_landmarks_for_mobile.dat");
            Run ();
            #endif
        }

        private void Run ()
        {
            faceLandmarkDetector = new FaceLandmarkDetector (shape_predictor_68_face_landmarks_dat_filepath);
    
            StartCoroutine (Initialize ());
        }

        private IEnumerator Initialize ()
        {
            if (webCamTexture != null) {
                webCamTexture.Stop ();
                hasInitDone = false;
            }
            // Checks how many and which cameras are available on the device
            for (int cameraIndex = 0; cameraIndex < WebCamTexture.devices.Length; cameraIndex++) {
                if (WebCamTexture.devices [cameraIndex].isFrontFacing == shouldUseFrontCamera) {
                    
                    Debug.Log (cameraIndex + " name " + WebCamTexture.devices [cameraIndex].name + " isFrontFacing " + WebCamTexture.devices [cameraIndex].isFrontFacing);
                    
                    webCamDevice = WebCamTexture.devices [cameraIndex];
                    
                    webCamTexture = new WebCamTexture (webCamDevice.name, width, height);
                    
                    break;
                }
            }
            
            if (webCamTexture == null) {
                //          Debug.Log ("webCamTexture is null");
                if (WebCamTexture.devices.Length > 0) {
                    webCamDevice = WebCamTexture.devices [0];
                    webCamTexture = new WebCamTexture (webCamDevice.name, width, height);
                } else {
                    webCamTexture = new WebCamTexture (width, height);
                }
            }
            
            Debug.Log ("width " + webCamTexture.width + " height " + webCamTexture.height + " fps " + webCamTexture.requestedFPS);

            // Starts the camera
            webCamTexture.Play ();

            while (true) {
                //If you want to use webcamTexture.width and webcamTexture.height on iOS, you have to wait until webcamTexture.didUpdateThisFrame == 1, otherwise these two values will be equal to 16. (http://forum.unity3d.com/threads/webcamtexture-and-error-0x0502.123922/)
                #if UNITY_IOS && !UNITY_EDITOR && (UNITY_4_6_3 || UNITY_4_6_4 || UNITY_5_0_0 || UNITY_5_0_1)
                if (webCamTexture.width > 16 && webCamTexture.height > 16) {
                #else
                if (webCamTexture.didUpdateThisFrame) {
                    //#if UNITY_IOS && !UNITY_EDITOR && UNITY_5_2                                    
                    while (webCamTexture.width <= 16) {
                        webCamTexture.GetPixels32 ();
                        yield return new WaitForEndOfFrame ();
                    } 
                    //#endif
                    #endif
                        
                    Debug.Log ("width " + webCamTexture.width + " height " + webCamTexture.height + " fps " + webCamTexture.requestedFPS);
                    Debug.Log ("videoRotationAngle " + webCamTexture.videoRotationAngle + " videoVerticallyMirrored " + webCamTexture.videoVerticallyMirrored + " isFrongFacing " + webCamDevice.isFrontFacing);
                        
                    colors = new Color32[webCamTexture.width * webCamTexture.height];
                        
                    texture2D = new Texture2D (webCamTexture.width, webCamTexture.height, TextureFormat.RGBA32, false);
                        
                    gameObject.GetComponent<Renderer> ().material.mainTexture = texture2D;
                        
                    UpdateLayout ();
                        
                    screenOrientation = Screen.orientation;
                    hasInitDone = true;
                        
                    break;
                } else {
                    yield return 0;
                }
            }
        }

        public IEnumerator Initialize(WebCamTexture wct)
        {
            webCamDevice = webCamDevice = WebCamTexture.devices[0];


            webCamTexture = wct;

            hasInitDone = true;

            //if (webCamTexture != null)
            //{
            //    webCamTexture.Stop();
            //    hasInitDone = false;
            //}

            //Debug.Log("width " + webCamTexture.width + " height " + webCamTexture.height + " fps " + webCamTexture.requestedFPS);

            // Starts the camera
            webCamTexture.Play();

            while (true)
            {
                //If you want to use webcamTexture.width and webcamTexture.height on iOS, you have to wait until webcamTexture.didUpdateThisFrame == 1, otherwise these two values will be equal to 16. (http://forum.unity3d.com/threads/webcamtexture-and-error-0x0502.123922/)
#if UNITY_IOS && !UNITY_EDITOR && (UNITY_4_6_3 || UNITY_4_6_4 || UNITY_5_0_0 || UNITY_5_0_1)
                if (webCamTexture.width > 16 && webCamTexture.height > 16) {
#else
                if (webCamTexture.didUpdateThisFrame)
                {

                    //#if UNITY_IOS && !UNITY_EDITOR && UNITY_5_2                                    
                    while (webCamTexture.width <= 16)
                    {
                        webCamTexture.GetPixels32();

                        yield return new WaitForEndOfFrame();
                    }
                    //#endif
#endif

                    Debug.Log("width " + webCamTexture.width + " height " + webCamTexture.height + " fps " + webCamTexture.requestedFPS);
                    Debug.Log("videoRotationAngle " + webCamTexture.videoRotationAngle + " videoVerticallyMirrored " + webCamTexture.videoVerticallyMirrored + " isFrongFacing " + webCamDevice.isFrontFacing);

                    colors = new Color32[webCamTexture.width * webCamTexture.height];

                    texture2D = new Texture2D(webCamTexture.width, webCamTexture.height, TextureFormat.RGBA32, false);

                    gameObject.GetComponent<Renderer>().material.mainTexture = texture2D;

                    //UpdateLayout();

                    screenOrientation = Screen.orientation;
                    hasInitDone = true;

                    break;
                }
                else
                {
                    yield return 0;
                }
            }
        }

        private void UpdateLayout ()
        {
            gameObject.transform.localRotation = new Quaternion (0, 0, 0, 0);
            gameObject.transform.localScale = new Vector3 (webCamTexture.width, webCamTexture.height, 1);

            if (webCamTexture.videoRotationAngle == 90 || webCamTexture.videoRotationAngle == 270)
            {
                gameObject.transform.eulerAngles = new Vector3(0, 0, -90);
            }

            float width = 0;
            float height = 0;
            if (webCamTexture.videoRotationAngle == 90 || webCamTexture.videoRotationAngle == 270) {
                width = gameObject.transform.localScale.y;
                height = gameObject.transform.localScale.x;
            } else if (webCamTexture.videoRotationAngle == 0 || webCamTexture.videoRotationAngle == 180) {
                width = gameObject.transform.localScale.x;
                height = gameObject.transform.localScale.y;
            }
                
            float widthScale = (float)Screen.width / width;
            float heightScale = (float)Screen.height / height;
            if (widthScale < heightScale) {
                Camera.main.orthographicSize = (width * (float)Screen.height / (float)Screen.width) / 2;
            } else {
                Camera.main.orthographicSize = height / 2;
            }

            flip = true;
            Debug.Log("ROTATION: " + webCamTexture.videoRotationAngle);
            Debug.Log("FRONT FACING: " + webCamDevice.isFrontFacing);
            if (webCamDevice.isFrontFacing) {
                if (webCamTexture.videoRotationAngle == 90 ) {
                    flip = !flip;
                }
                if (webCamTexture.videoRotationAngle == 180) {
                    flip = !flip;
                }
            } else {
                if (webCamTexture.videoRotationAngle == 180) {
                    flip = !flip;
                } else if (webCamTexture.videoRotationAngle == 270) {
                    flip = !flip;
                }
            }
            

            if (!flip) {
                //transform.rotation = transform.rotation * Quaternion.AngleAxis(webCamTexture.videoRotationAngle, Vector3.up);
                gameObject.transform.localScale = new Vector3 (gameObject.transform.localScale.x, -gameObject.transform.localScale.y, 1);
            }
            //Quaternion rot = Quaternion.Euler(0, 0, Time.time * 30);
            //Matrix4x4 m = Matrix4x4.TRS(Vector3.zero, rot, Vector3.one);
            //GetComponent<Renderer>().material.SetMatrix("_TextureRotation", m);
        }

        // Update is called once per frame
        void Update ()
        {
            if (!hasInitDone)
                return;
                
            if (screenOrientation != Screen.orientation) {
                screenOrientation = Screen.orientation;
                UpdateLayout ();
            }

            #if UNITY_IOS && !UNITY_EDITOR && (UNITY_4_6_3 || UNITY_4_6_4 || UNITY_5_0_0 || UNITY_5_0_1)
            if (webCamTexture.width > 16 && webCamTexture.height > 16) {
            #else
            if (webCamTexture.didUpdateThisFrame) {
                #endif
                
                webCamTexture.GetPixels32 (colors);
                
                colors = RotateTexture(colors, webCamTexture.height, webCamTexture.width);
                texture2D.Resize(webCamTexture.height, webCamTexture.width);
                texture2D.SetPixels32(colors);
                texture2D.Apply();

                faceLandmarkDetector.SetImage<Color32> (colors, texture2D.width, texture2D.height, 4, flip);
        
                //detect face rects
                List<Rect> detectResult = faceLandmarkDetector.Detect ();

                //draw face rect
                var test = faceLandmarkDetector.DetectRectDetection();
                if (test != null && test.Count > 0)
                {
                    var sliceTexture = TextureSlice(texture2D, test[0].rect);
                    CameraFeed.texture = sliceTexture;
                }

                // Show the detected face details on the texture 

                foreach (var rect in detectResult)
                {
                    //Debug.Log ("face : " + rect);

                    //detect landmark points
                    var a = faceLandmarkDetector.DetectLandmark(rect);
                    //TEST_SceneTransition.FacePointData(faceLandmarkDetector.DetectLandmark(rect));
                    //draw landmark points

                    faceLandmarkDetector.DrawDetectLandmarkResult<Color32>(colors, texture2D.width, texture2D.height, 4, flip, 0, 255, 0, 255);
                    
                    //NormalizedPoints(a);
                }



                ////Debug.Log(test[0].rect.x + ", " + test[0].rect.y + ", " + test[0].rect.width + "x" + test[0].rect.height);
                //faceLandmarkDetector.DrawDetectResult<Color32> (colors, webCamTexture.width, webCamTexture.height, 4, flip, 255, 0, 0, 255, 2);


                texture2D.SetPixels32 (colors);
                texture2D.Apply ();
            }
        }

        public Color32[] RotateTexture(Color32[] tex, int height, int width)
        {
            var colorResult = new Color32[tex.Length];

            int count = 0;
            int newWidth = height;
            int newHeight = width;
            int index = 0;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    index = (width * (height - j)) - width + i;

                    colorResult[count] = tex[index];
                    count++;
                }
            }

            return colorResult;
        }


       
        // Set the texture slice for a texture
        private Texture2D TextureSlice(Texture2D texture2D, Rect sliceRect)
        {
            const float posMin = 0.1f;
            const float scaleMin = 0.05f;

            const float posMultiplier = 0.25f;
            const float scaleMultiplier = 0.15f;
            if (_texture2D != texture2D || _sliceRect == null)
            {
                // new Texture
                _texture2D = texture2D;
                _sliceRect = sliceRect;
            }
            else
            {
                // transition slice rect positioning from old to new slice rect location
                _sliceRect.x = Mathf.MoveTowards(_sliceRect.x, sliceRect.x, Mathf.Max(Mathf.Abs(_sliceRect.x - sliceRect.x) * posMultiplier, posMin));
                _sliceRect.y = Mathf.MoveTowards(_sliceRect.y, sliceRect.y, Mathf.Max(Mathf.Abs(_sliceRect.y - sliceRect.y) * posMultiplier, posMin));

                _sliceRect.width = Mathf.MoveTowards(_sliceRect.width, sliceRect.width, Mathf.Max(Mathf.Abs(_sliceRect.width - sliceRect.width) * scaleMultiplier, scaleMin));
                _sliceRect.height = Mathf.MoveTowards(_sliceRect.height, sliceRect.height, Mathf.Max(Mathf.Abs(_sliceRect.height - sliceRect.height) * scaleMultiplier, scaleMin));
            }

            return GetFaceTexture(_texture2D, _sliceRect);
        }

        private Texture2D GetFaceTexture(Texture2D texture, Rect sliceRect)
        {
            var width = Mathf.RoundToInt(sliceRect.width);
            var height = Mathf.RoundToInt(sliceRect.height);

            // Make sure the sprite is a square
            width = Mathf.Max(width, height);
            height = width;

            //Add padding
            var padding = Mathf.RoundToInt(width * 0.25f);
            width += padding;
            height += padding;
            sliceRect.x -= padding / 2;
            sliceRect.y -= padding / 2;

            var startX = Mathf.RoundToInt(sliceRect.x);
            var startY = texture.height - Mathf.RoundToInt(sliceRect.y);

            var endX = startX + width;
            var endY = startY - height;

            // make sure x doesnt go past the edges
            if (startX < 0)
            {
                endX += startX * -1;
                startX = 0;
            }
            if (endX > texture.width)
            {
                startX -= (endX - texture.width);
                endX = texture.width;
            }
            
            // make sure y doesnt go past the edges
            if (endY < 0)
            {
                startY += endY * -1;
                endY = 0;
            }
            if (startY > texture.height)
            {
                endY -= (startY- texture.height);
                startY = texture.height;
            }

            var newTexture = new Texture2D(width, height);
            var c = new Color32[width * height];

            var index = 0;
            for (int j = endY; j < startY; j++)
            {
                for (int i = startX; i < endX; i++)
                {
                    c[index] = texture.GetPixel(i, j);
                    index++;
                }
            }

            newTexture.SetPixels32(c);
            newTexture.Apply();

            return newTexture;
        }

        private void NormalizedPoints(List<Vector2> points)
        {
            // our nose
            var left = points[36];
            // our chin
            var right = points[45];

            var center = new Vector2((left.x + right.x) / 2f, (left.y + right.y) / 2f);

            var scale = 1 / Vector2.Distance(left, right);

            var normalized = new List<Vector2>();

            foreach (var point in points)
            {
                var x = (point.x - center.x) * scale;
                var y = (point.y - center.y) * scale;

                //Vector3 vect = new Vector3(point.x, point.y, 0);
                //Vector3 vectt = vect.inverseTransform

                normalized.Add(new Vector2(x, y));
            }

            TEST_SceneTransition.FacePointData(normalized);
            TEST_SceneTransition.ShowFacePoints(normalized);

        }

        public void Tick(WebCamTexture wct)
        {
            if (wct != null && screenOrientation != Screen.orientation)
            {
                screenOrientation = Screen.orientation;
                UpdateLayout();
            }
            wct.GetPixels32(colors);

            if (faceLandmarkDetector == null)
            {
                shape_predictor_68_face_landmarks_dat_filepath =
                    Utils.getFilePath("shape_predictor_68_face_landmarks.dat");
                faceLandmarkDetector = new FaceLandmarkDetector(shape_predictor_68_face_landmarks_dat_filepath);
            }
            faceLandmarkDetector.SetImage<Color32>(colors, wct.width, wct.height, 4, flip);

            //detect face rects
            List<Rect> detectResult = faceLandmarkDetector.Detect();

            foreach (var rect in detectResult)
            {
                Debug.Log(rect);
                //detect landmark points
                faceLandmarkDetector.DetectLandmark(rect);

                //draw landmark points
                faceLandmarkDetector.DrawDetectLandmarkResult<Color32>(colors, wct.width, wct.height, 4, flip, 0, 255, 0, 255);
            }
            //draw face rect
            faceLandmarkDetector.DrawDetectResult<Color32>(colors, wct.width, wct.height, 4, flip, 255, 0, 0, 255, 2);

            texture2D.SetPixels32(colors);
            texture2D.Apply();
        }

        /// <summary>
        /// Raises the destroy event.
        /// </summary>
        void OnDestroy ()
        {
            if (webCamTexture != null)
                webCamTexture.Stop ();
            if (faceLandmarkDetector != null)
                faceLandmarkDetector.Dispose ();

            #if UNITY_WEBGL && !UNITY_EDITOR
            foreach (var coroutine in coroutines) {
                StopCoroutine (coroutine);
                ((IDisposable)coroutine).Dispose ();
            }
            #endif
        }

        /// <summary>
        /// Raises the back button click event.
        /// </summary>
        public void OnBackButtonClick ()
        {
            #if UNITY_5_3 || UNITY_5_3_OR_NEWER
            SceneManager.LoadScene ("DlibFaceLandmarkDetectorExample");
            #else
            Application.LoadLevel ("DlibFaceLandmarkDetectorExample");
            #endif
        }
        
        /// <summary>
        /// Raises the change camera button click event.
        /// </summary>
        public void OnChangeCameraButtonClick ()
        {
            shouldUseFrontCamera = !shouldUseFrontCamera;
            StartCoroutine (Initialize ());
        }
    }
}