﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//[ExecuteInEditMode]
public class SetOrientation : MonoBehaviour
{
    public float LandscapeAspectRatio = 1f;
    public int HorizontalColumnConstraint = 4;
    public int VerticalColumnConstraint = 2;
    public bool ForceSquare;
    
    public bool HorizontalAlignment;

    private AspectRatioFitter _aspectRatioFitter;
    private RectTransform _rectTransform;
    private GridLayoutGroup _gridLayoutGroup;

	// Use this for initialization
	void Start ()
	{
	    _aspectRatioFitter = GetComponent<AspectRatioFitter>();
	    _rectTransform = GetComponent<RectTransform>();
	    _gridLayoutGroup = GetComponentInChildren<GridLayoutGroup>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    var squareConstraint = Mathf.RoundToInt(Mathf.Sqrt(_gridLayoutGroup.transform.childCount));

	    if (HorizontalAlignment)
	    {
	        // should be horizontally aligned
            _aspectRatioFitter.aspectMode = AspectRatioFitter.AspectMode.WidthControlsHeight;
	        _aspectRatioFitter.aspectRatio = 1f * LandscapeAspectRatio;
	        _gridLayoutGroup.constraintCount = ForceSquare ? squareConstraint : HorizontalColumnConstraint;
            _gridLayoutGroup.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
	    }
        else
	    {
	        // should be vertically aligned
            _aspectRatioFitter.aspectMode = AspectRatioFitter.AspectMode.HeightControlsWidth;
	        _aspectRatioFitter.aspectRatio = 1f / LandscapeAspectRatio;
	        _gridLayoutGroup.constraintCount = ForceSquare ? squareConstraint : VerticalColumnConstraint;
	        _gridLayoutGroup.constraint = ForceSquare ? GridLayoutGroup.Constraint.FixedRowCount : GridLayoutGroup.Constraint.FixedColumnCount;
        }

        _rectTransform.offsetMax = Vector2.zero;
        _rectTransform.offsetMin = Vector2.zero;
        _rectTransform.position = Vector3.zero;
	    
	}
}
